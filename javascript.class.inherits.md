    Function.prototype.inherits = function(parent) {
      this.prototype = Object.create(parent.prototype);
    };

    function Monster(props) {
      this.health = props.health || 100;
    }

    Monster.prototype = {
      growl: function() {
        return 'Grrrrr';
      }
    };

    Monkey.inherits(Monster);
    function Monkey() {
      Monster.apply(this, arguments);
    }

    var monkey = new Monkey({ health: 200 });

    console.log(monkey.health); //=> 200
    console.log(monkey.growl()); //=> "Grrrr"
